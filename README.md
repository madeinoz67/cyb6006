# CYB6006 - Assignment 2

## Purpose

To write two utilities in language of choice:

1. Dictionary based password cracker
2. Password cracker

## Language

Python 3.7.x

## Requirements

To install the library requirements:

`pip install -r requirements.txt`

## programs

### crak

see <crak/README.md>

### skan

see <skan/README.md>

## Project Tree

```
.
├── README.md
├── crak
│   ├── README.md
│   ├── crak.py
│   ├── dicttest.txt
│   ├── list.txt
│   ├── notebook.ipynb
│   └── shadow
├── requirements.txt
└── skan
    ├── README.md
    └── skan.py
```